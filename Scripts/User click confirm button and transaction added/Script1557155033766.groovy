import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://3.85.99.75:8085/')

WebUI.setText(findTestObject('Page_ProjectBackend/input_Username_username'), 'user')

WebUI.setEncryptedText(findTestObject('Page_ProjectBackend/input_Password_password'), '1/VWEm4uipk=')

WebUI.click(findTestObject('Page_ProjectBackend/button_Login'))

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart'))

WebUI.click(findTestObject('Page_ProjectBackend/a_Carts            1'))

WebUI.click(findTestObject('Page_ProjectBackend/td_Garden'))

WebUI.click(findTestObject('Page_ProjectBackend/td_20000 THB'))

WebUI.setText(findTestObject('Page_ProjectBackend/input_Garden_amount'), '1')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/p_Total price  20000 THB'), 'Total price: 20,000 THB')

WebUI.setText(findTestObject('Page_ProjectBackend/input_Garden_amount'), '2')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/p_Total price  40000 THB'), 'Total price: 40,000 THB')

WebUI.click(findTestObject('Page_ProjectBackend/button_confirm'))

WebUI.acceptAlert()

WebUI.click(findTestObject('Page_ProjectBackend/button_Logout'))

WebUI.delay(1)

WebUI.setText(findTestObject('Page_ProjectBackend/input_Username_username'), 'admin')

WebUI.setEncryptedText(findTestObject('Page_ProjectBackend/input_Password_password'), 'RAIVpflpDOg=')

WebUI.click(findTestObject('Page_ProjectBackend/button_Login'))

WebUI.click(findTestObject('Page_ProjectBackend/a_Total Transaction'))

WebUI.click(findTestObject('Page_ProjectBackend/td_Garden'))

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/td_40000 THB'), '40,000 THB')

WebUI.closeBrowser()

