<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>test to test login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>44dc2678-6250-46d3-8636-1a4de31cd698</testSuiteGuid>
   <testCaseLink>
      <guid>78334e6a-9ce1-487b-b981-0e78d2ea99e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/test login/Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>1e518cdf-3622-498a-a5ba-2a43a212408f</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/to test login</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>1e518cdf-3622-498a-a5ba-2a43a212408f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>7eb30e01-43f7-4575-a27c-56c6337233f2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1e518cdf-3622-498a-a5ba-2a43a212408f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>58ed2fec-bfbe-4b18-96cf-a43b7e10f778</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
